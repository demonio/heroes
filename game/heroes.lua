local hero = {}

hero.test = {
            zivoty = 50, 
            mana = 20,
            speed = 1.5,
            name = "test",
            skill1 = skill.melle,
            skill2 = skill.blizzard,
            skill3 = skill.dragon_blade,
            skill4 = skill.cure,
            skill5 = skill.thunder,
            IMG = love.graphics.newImage("graphics/player/test.png")
  }

return hero