local skill = {}
 skill.bg = love.graphics.newImage("graphics/skills/bg.png")
 skill.melle = {dmg = 2,
                stun = 0,
                imobilize = 0,
                cooldown = 60,
                speed = 0,
                mana = 0,
                range = 1,
                IMG = love.graphics.newImage("graphics/skills/1.png")
                }

skill.blizzard = {dmg = 0,
                  stun = 120,
                  imobilize = 0,
                  cooldown = 60,
                  speed = 0,
                  mana = 10,
                  range = 1.5,
                  IMG = love.graphics.newImage("graphics/skills/blizzard-1.png")
                }
skill.dragon_blade = {dmg = 6,
                      stun = 0,
                      imobilize = 0,
                      cooldown = 120,
                      speed = 0,
                      mana = 12,
                      range = 1,
                      IMG = love.graphics.newImage("graphics/skills/dragon-blade-1.png")
                    }
skill.cure = {dmg = -5,
                  stun = 0,
                  imobilize = 0,
                  cooldown = 90,
                  speed = 0,
                  mana = 20,
                  range = 2,
                  IMG = love.graphics.newImage("graphics/skills/cure-1.png")
                }
skill.thunder = {dmg = 4,
                  stun = 0,
                  imobilize = 30,
                  cooldown = 240,
                  speed = 0,
                  mana = 40,
                  range = 2.5,
                  IMG = love.graphics.newImage("graphics/skills/thunder-1.png")
                }
                

return skill