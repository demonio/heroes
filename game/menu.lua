local menu = {}

local tlacitka = require("buttons")

function menu.update(dt)
  tlacitka.clear()
  tlacitka.spawn(width / 2 - 100, height / 2 - 100, language.vs_bot, 2)
  tlacitka.spawn(width / 2 - 100, height / 2 - 50, language.vs_player, 3)
  tlacitka.spawn(width / 2 - 100, height / 2, language.shop, 4)
  tlacitka.spawn(width / 2 - 100, height / 2 + 50, language.character, 5)
  tlacitka.spawn(width / 2 - 100, height / 2 + 100, language.quit, 99)
  tlacitka.check()
end

function menu.mousepressed(x, y, button)
  if button == "l" then
    if tlacitka.click(x, y) then
      if tlacitka.click(x, y) == 99 then state = 3 else
      step_id = tlacitka.click(x, y)
      end
    end
  end
end

function menu.keypressed(key)
  if key == "escape" then
    if step_id > 1 then
      step_id = 1 
    else
      love.event.quit()
    end
  end
end

function menu.draw()
  tlacitka.draw()
end

return menu