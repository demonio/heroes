local bot = {}

local pregame = true

local srdce = love.graphics.newImage("graphics/hud/srdce.png")

local move_tmp = 0

local enemy = {}

local spawn_timer = 0

local tx, ty = 0, 0

function npc_pocet(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function npc_spawn()
  if npc_pocet(enemy) < 20 then
    math.randomseed(os.time())
    table.insert(enemy, {x = math.random(64,1920), y = math.random(64,1920), IMG = love.graphics.newImage("graphics/npc/".. math.random(1,24) ..".png"), zivoty = 20})
  end
end

function npc_attack()

end

function npc_move()
  for i, v in ipairs(enemy) do
    v.x = v.x + math.random(-64,64)
    v.y = v.y + math.random(-64,64)
  end
end

function npc_draw()
  for i, v in ipairs(enemy) do
    if v.zivoty > 5 then
      love.graphics.print( v.zivoty .. "x", v.x + v.IMG:getWidth()/2 - 40 , v.y - 30)
      love.graphics.draw( srdce, v.x + v.IMG:getWidth()/2 + 10 , v.y - 20)
    else
    for i = 1, v.zivoty, 1 do
      love.graphics.draw(srdce, (v.x + v.IMG:getWidth()/2) - v.zivoty/2 * srdce:getWidth() + ((i-1)*srdce:getWidth()) , v.y - 20)
    end end
		love.graphics.draw(v.IMG, v.x, v.y) 
	end
end

function bot.update(dt)
  if spawn_timer < 600 then
    spawn_timer = spawn_timer + 1
  else
    npc_spawn()
    spawn_timer = 0
  end
  if move_tmp < 15 then
    move_tmp = move_tmp + 1
  else
    npc_move()
    move_tmp = 0
  end
  if ty < 0 and love.keyboard.isDown("up") then ty = ty + 250*dt end
  if ty > (-map[map_id].height*map[map_id].tileHeight  + height) and love.keyboard.isDown("down") then ty = ty - 250*dt end
  if tx < 0 and love.keyboard.isDown("left") then tx = tx + 250*dt end
  if tx > (-map[map_id].width*map[map_id].tileWidth + width) and love.keyboard.isDown("right") then tx = tx - 250*dt end
end

function bot.draw()
  love.graphics.translate( math.floor(tx), math.floor(ty) )
  map[map_id]:autoDrawRange( math.floor(tx), math.floor(ty), 1, pad)
  map[map_id]:draw()
  npc_draw()
  love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
end

return bot
