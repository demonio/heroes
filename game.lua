local game = {}

setup = 0

-- jednotlive oknaa prace s nimi
widget = {}
widget_id = 0

-- jednotlive stadia hry
step = {require("game/menu"), require("game/vs_bot")}
step_id = 1

local bg = love.graphics.newImage("graphics/bg/brown.png")
bg:setWrap("repeat", "repeat")

function game.update(dt)
  if setup == 0 then
    player = {
      name = love.filesystem.read("user.name"),
      atrib = love.filesystem.read("user.atrib"),
      nation = love.filesystem.read("user.nation")
    }
    setup = 1
  end
  quad = love.graphics.newQuad(0, 0, width, height, 64, 64)
  if step_id > 0 and step[step_id].update then step[step_id].update(dt) end
  if widget_id > 0 and widget[widget_id].update then widget[widget_id].update(dt) end
end

function game.draw()
  love.graphics.draw(bg, quad, 0, 0)
  if step_id > 0 and step[step_id].draw then step[step_id].draw() end
  if widget_id > 0 and widget[widget_id].draw then widget[widget_id].draw() end
end

function game.mousepressed(x, y, button)
  if step_id > 0 and step[step_id].mousepressed then step[step_id].mousepressed(x, y, button) end
  if widget_id > 0 and widget[widget_id].mousepressed then widget[widget_id].mousepressed(x, y, button) end
end

function game.keypressed(key)
  if step_id > 0 and step[step_id].keypressed then step[step_id].keypressed(key) end
  if widget_id > 0 and widget[widget_id].keypressed then widget[widget_id].keypressed(key) end
end

return game