if not love.filesystem.exists("user.name") then  
  state = 1
else
  state = 2
end

ATL = require("AdvTiledLoader")
ATL.Loader.path = 'maps/'
map = {ATL.Loader.load("test.tmx")}
map_id = 1

-- nastavení engine
gamestate = {require("new"), require("game"), require("quit")}
language = require("text/language")

menuf = love.graphics.newFont("graphics/fonts/menu.otf", 20)
love.graphics.setFont( menuf )

--audio funkce
do
    -- will hold the currently playing sources
    local sources = {}

    -- check for sources that finished playing and remove them
    -- add to love.update
    function love.audio.update()
        local remove = {}
        for _,s in pairs(sources) do
            if s:isStopped() then
                remove[#remove + 1] = s
            end
        end

        for i,s in ipairs(remove) do
            sources[s] = nil
        end
    end

    -- overwrite love.audio.play to create and register source if needed
    local play = love.audio.play
    function love.audio.play(what, how, loop)
        local src = what
        if type(what) ~= "userdata" or not what:typeOf("Source") then
            src = love.audio.newSource(what, how)
            src:setLooping(loop or false)
        end

        play(src)
        sources[src] = src
        return src
    end

    -- stops a source
    local stop = love.audio.stop
    function love.audio.stop(src)
        if not src then return end
        stop(src)
        sources[src] = nil
    end
end

function love.load()
  print("\n---- SUPPORTED ---- ");
  print("Canvas:         " .. tostring(love.graphics.isSupported('canvas')));
  print("PO2:            " .. tostring(love.graphics.isSupported('npot')));
  print("Subtractive BM: " .. tostring(love.graphics.isSupported('subtractive')));
  print("Shaders:        " .. tostring(love.graphics.isSupported('shader')));
  print("HDR Canvas:     " .. tostring(love.graphics.isSupported('hdrcanvas')));
  print("Multicanvas:    " .. tostring(love.graphics.isSupported('multicanvas')));
  print("Mipmaps:        " .. tostring(love.graphics.isSupported('mipmap')));
  print("DXT:            " .. tostring(love.graphics.isSupported('dxt')));
  print("BC5:            " .. tostring(love.graphics.isSupported('bc5')));
  print("SRGB:           " .. tostring(love.graphics.isSupported('srgb')));

  print("\n---- RENDERER  ---- ");
  local name, version, vendor, device = love.graphics.getRendererInfo()
  print(string.format("Name: %s \nVersion: %s \nVendor: %s \nDevice: %s", name, version, vendor, device));
  --bgm = love.audio.play("audio/bg.mp3", "stream", true)
  min_dt = 1/60
  next_time = love.timer.getTime()
  if gamestate[state].load then gamestate[state].load() end
end

-- Vkládání textu do polí
function love.textinput(t)
	-- update gamestate
	if gamestate[state].textinput then gamestate[state].textinput(t) end
end

function love.update(dt)
  --love.audio.update()
  mousex = love.mouse.getX()
  mousey = love.mouse.getY()
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  next_time = next_time + min_dt
  if gamestate[state].update then gamestate[state].update(dt) end
end

function love.draw()
  if gamestate[state].draw then gamestate[state].draw() end
  local cur_time = love.timer.getTime()
  if next_time <= cur_time then
    next_time = cur_time
    return
  end
  love.timer.sleep(next_time - cur_time)
end

function love.mousepressed(x, y, button)
  if gamestate[state].mousepressed then gamestate[state].mousepressed(x, y, button) end
end

function love.keypressed(key)
  if key == "f11" then
  toggle = not toggle
	love.window.setFullscreen(toggle, "desktop")
  end
  if gamestate[state].keypressed then gamestate[state].keypressed(key) end
end

function love.textinput(t)
  if gamestate[state].textinput then gamestate[state].textinput(t) end
end
