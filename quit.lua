local quit = {}

function quit.update(dt)
	love.event.quit()
end

return quit
