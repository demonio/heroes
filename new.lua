local new = {}
local tlacitka = require("buttons")

local kenney = love.graphics.newImage("graphics/kenney.png")
local venca = love.graphics.newImage("credits.png")
local inputbg = love.graphics.newImage("graphics/ui/input.png")

local bg = love.graphics.newImage("graphics/bg/green.png")
bg:setWrap("repeat", "repeat")

local local_state = 1
local prezdivka = ""

function new.update(dt)
  quad = love.graphics.newQuad(0, 0, width, height, 64, 64)
	tlacitka.clear()
  if local_state == 1 then
    tlacitka.spawn(width / 2 - 100, height / 2 + 100, language.continue, language.continue)
  elseif local_state == 2 then
    tlacitka.spawn(width / 2 - 100, height / 2 - 100, language.nation1, language.nation1)
    tlacitka.spawn(width / 2 - 100, height / 2, language.nation2, language.nation2)
    tlacitka.spawn(width / 2 - 100, height / 2 + 100, language.nation3, language.nation3)
  elseif local_state == 3 then
    tlacitka.spawn(width / 2 - 100, height / 2 - 100, language.str, language.str)
    tlacitka.spawn(width / 2 - 100, height / 2, language.int, language.int)
    tlacitka.spawn(width / 2 - 100, height / 2 + 100, language.agl, language.agl)
  end
	tlacitka.check()
end

function new.textinput(t)
  if local_state == 1 then
    prezdivka = prezdivka .. t
  end
end

function new.draw()
  love.graphics.draw(bg, quad, 0, 0)
  tlacitka.draw()
  if local_state == 1 then
    love.graphics.setColor(0, 0, 0)
    love.graphics.printf(language.enter_name, (width / 2) - (menuf:getWidth(language.enter_name) / 2), (height / 2) - 100, love.graphics.getWidth())
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(inputbg,(width / 2) - (inputbg:getWidth() / 2), (height / 2) - (inputbg:getHeight() / 3) )
    love.graphics.printf(prezdivka, (width / 2) - (menuf:getWidth(prezdivka) / 2), (height / 2), love.graphics.getWidth()/2)
  elseif local_state == 2 then
    love.graphics.setColor(0, 0, 0)
    love.graphics.printf(language.choose_nation, (width / 2) - (menuf:getWidth(language.choose_nation) / 2), (height / 2) - 200, love.graphics.getWidth())
    love.graphics.setColor(255, 255, 255)
  elseif local_state == 3 then
    love.graphics.setColor(0, 0, 0)
    love.graphics.printf(language.choose_atrib, (width / 2) - (menuf:getWidth(language.choose_atrib) / 2), (height / 2) - 200, love.graphics.getWidth())
    love.graphics.setColor(255, 255, 255)
  end
end

function new.mousepressed(x, y, button)
	if button == "l" then
		if tlacitka.click(x, y) == language.continue and local_state == 1 then
      love.filesystem.newFile("user.name")
      love.filesystem.write("user.name", prezdivka)
      local_state = 2
      return
    end
    if tlacitka.click(x, y) and local_state == 2 then
      love.filesystem.newFile("user.nation")
      love.filesystem.write("user.nation", tlacitka.click(x, y))
      local_state = 3
      return
    end
    if tlacitka.click(x, y) and local_state == 3 then
      love.filesystem.newFile("user.atrib")
      love.filesystem.write("user.atrib", tlacitka.click(x, y))
      state = 2
      return
    end
	end
end

function new.keypressed(key)
  if local_state == 1 then
    if key == "backspace" then
      prezdivka = ""
    end
    if key == "return" then
      love.filesystem.newFile("user.name")
      love.filesystem.write("user.name", prezdivka)
      local_state = 2
    end
  end
	if key == "escape" then
		state = 3
	end
end
return new